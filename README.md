### Whoosh - Yet another command line web scraper
### Author: Dellon Zerus
### All code licensed under GPL

Whoosh is a small command line utility written in Python, using bs4 to scrape web pages. 

## Features:
- Usable from command line
- Write output to html file
- Scan for web services
- Add your own web services to scan
- Edit and save pages
- Crawl links from website

And loads more stuff I'm probably forgetting.

## Use the Scan feature to detect:
- Analytics
- Fonts
- Programming Languages
- Comments
- Styles

## Usage examples:
# Output contents of every paragraph:
`whoosh --link www.example.com --tag p --attribute style`

# Output every link:
`whoosh -t a -a href www.example.com`

# Scan website for analytics services
`whoosh -s -sv analytics www.example.com`

## Dependencies:
- python3 - The language
- pip3 - The installer (not necessary if you can install the modules manually)
- bs4 - For the actual scraping
- requests - For getting the webpage
- argparse - For handling the command-line arguments



## Installation instructions:
```
sudo -i
git clone https://github.com/DZerus/whoosh
cd whoosh
chmod +x install.sh && ./install.sh
whoosh
```