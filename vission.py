#!/usr/bin/python3
# Made by: Dellon Zerus AKA Aleksei S.

__version__ = "1.0 Alpha"

from sys import exit

#Logo
banner = """ 
\033[91m
 ██▒   █▓ ██▓  ██████   ██████  ██▓ ▒█████   ███▄    █
▓██░   █▒▓██▒▒██    ▒ ▒██    ▒ ▓██▒▒██▒  ██▒ ██ ▀█   █
 ▓██  █▒░▒██▒░ ▓██▄   ░ ▓██▄   ▒██▒▒██░  ██▒▓██  ▀█ ██▒
  ▒██ █░░░██░  ▒   ██▒  ▒   ██▒░██░▒██   ██░▓██▒  ▐▌██▒
   ▒▀█░  ░██░▒██████▒▒▒██████▒▒░██░░ ████▓▒░▒██░   ▓██░
   ░ ▐░  ░▓  ▒ ▒▓▒ ▒ ░▒ ▒▓▒ ▒ ░░▓  ░ ▒░▒░▒░ ░ ▒░   ▒ ▒
   ░ ░░   ▒ ░░ ░▒  ░ ░░ ░▒  ░ ░ ▒ ░  ░ ▒ ▒░ ░ ░░   ░ ▒░
     ░░   ▒ ░░  ░  ░  ░  ░  ░   ▒ ░░ ░ ░ ▒     ░   ░ ░
      ░   ░        ░        ░   ░      ░ ░           ░
     ░
\033[0m
\033[1mVission\033[0m - Yet another command line web scraper (this time with cool logo)

Made by Dellon Zerus AKA Aleksei S.
Version: {0}
"""

def main():
    checkArgs()
    scrape()
    return 

#Checks whether user passed arguments. Self explanatory.
def checkArgs():
    from sys import argv

    if len(argv) <= 1:
        print(banner.format(__version__))
        print("Looks like you didn't pass any arguments.")
        print("Try \033[1m./vission --help\033[0m for a manual.")
        exit(1)
    return


def scrape():
    from bs4 import BeautifulSoup
    args, link = parseArgs()

    #If user specified the --version argument, we do not need to do anything else
    if args.version == True:
        print(__version__)
        exit(0)
    
    #Retrieve HTML from either file or link (if both are specified it will only get the file)
    html = getHTML(args.fname, link)
    soup = BeautifulSoup(html, "html.parser")

    if args.scansw != False:
        scan(soup, args.scans)
        return
    saved = ""
    attrs = []
    #Ignore filtering by tag/class if ID filtering is on, since there can only be one element with a given ID
    if args.id != None:
        saved = soup.findAll(id=args.id)
        if str(saved) == "":
            print("The given ID was not found")
            exit(0)

    if args.tagname != None: # and args.id is None:
        saved = soup.findAll(args.tagname)
        if str(saved) == "":
            print("Given tag was not found")
            exit(0)

    if args.cls != None: # and args.id is None:
        if args.tagname != None:
            saved = soup.findAll(args.tagname, {"class":args.cls})
        else:
            saved = soup.findAll(class_ = args.cls)

    if args.attr != None:
        for element in saved:
            try:
                attrs.append(element.attrs[args.attr])
            except KeyError:
                pass
    if attrs:
        for item in attrs:
            print(i)
    else:
        for item in saved:
            print(i)

def getHTML(fname, link): 
    #If user specified a file to read...
    if fname != None:
        html = getFile(fname)
    
    #If user specified a link to get...
    elif link != None:
        html = getLink(link[0])

    #If there is no link and no file name    
    else:
        print("You forgot to specify a link or a filename. Either that or you did so incorrectly...")
        exit(1)
    return html

def scan(soup, service):
    from bs4 import BeautifulSoup
    return
    #TODO

def getLink(link):
    import requests
    
    #Check if the link has a schema and add one if it does not 
    if "http://" not in link and "https://" not in link:
        print("Your link did not contain a schema. Adding one right now...")
        link = "http://" + link
    
    #Check for invalid URL, or generic errors
    try:
        source = requests.get(link)
    
    except requests.exceptions.InvalidURL:
        print("You supplied an invalid URL. Are you sure you spelled {0} correctly?".format(link))
        exit(1)
    
    except Exception as e:
        print("An error occured while getting page", e)
        exit(1)
    
    print("Retrieved website at {0}.\n HTTP Status Code: {1}".format(link, source.status_code))

    return source.content


def getFile(fname):
    try:
        html = ""
        with open(fname, "r") as file:
            for line in file.readlines():
                html += line
        return html
    
    except FileNotFoundError:
        print("You supplied an invalid file path. Are you sure {0} is a real file?".format(fname))
        exit(1)
    
    except Exception as e:
        print("An error occured while reading from file: ", e)
        exit(1)



#Handles whatever user passed as argument. 
def parseArgs():
    import argparse

    parser = argparse.ArgumentParser(
            description="Vission - Yet another command line web scraper", 
            epilog="For more in-depth instruction check out the man page") 
    
    #Commented options may or may not be added since I am not sure how useful they will be.

    help_texts={
            "vrsn"   : "Output program version and exit.",
            "tag"    : "Indicate which tag you want to look for.",
            "class"  : "Search for elements matching given class.",
            "id"     : "Search for element with given id value. Not usable with -t or -c.",
            #"sattr"  : "Search for items with an attribute that matches given value",
            "attr"   : "Only output the value of given attribute. Not usable alone.",
            "file"   : "Specify html file to read. (note: advisable for repeated queries)",
            "edit"   : "Take take result, and replace with given value. (note: store between quotes and use with -o)",
            "output" : "Take edited html and output it to an html file.",
            "scan"   : "Indicate you want to scan website for presence of popular web services.",
            "scans"  : "Indicate a specific service type to scan for.",
            #"child"  : "Indicate that you would like to print children of found elements as well.",
            #"crawl"  : "Indicate you want to scrape any links from webpage as well.",
            #"climit" : "How many layers of links you want to crawl.",
            #"cdelay" : "Specify delay between crawling each link. Advisable if you're using a high crawler limit.",
            #"crandm" : "Just like -ccd, only with a random delay. Ranges from given value, to given value plus 15 seconds."
    }

    parser.add_argument("-v", "--version", action="store_true", dest="version", help=help_texts["vrsn"])
    parser.add_argument("-t", "--tag", action="store", dest="tagname", help=help_texts["tag"])
    parser.add_argument("-c", "--class", action="store", dest="cls", help=help_texts["class"])
    parser.add_argument("-i", "--id", action="store", dest="id", help=help_texts["id"])
    #parser.add_argument("-sa", "--search-attr", action="store", dest="sattr", help=help_texts["sattr"])
    parser.add_argument("-a", "--attr", action="store", dest="attr", help=help_texts["attr"])
    #parser.add_argument("-ch", "--children", action="store_true", dest="child", default=False, help=help_texts["child"])
    parser.add_argument("-f", "--file", action="store", dest="fname", help=help_texts["file"])
    parser.add_argument("-e", "--edit", action="store", dest="edit", help=help_texts["edit"])
    parser.add_argument("-o", "--output", action="store", dest="ofile",  help=help_texts["output"])
    parser.add_argument("-ss", "--scan", action="store_true", dest="scansw", default=False, help=help_texts["scan"])
    parser.add_argument("-ssv", "--scan-service", action="store", dest="scans", help=help_texts["scans"])
    #parser.add_argument("-cc", "--crawl", action="store_true", dest="crawl", default=False, help=help_texts["crawl"])
    #parser.add_argument("-ccl", "--crawl-limit", action="store", dest="climit", type=int, default=3, help=help_texts["climit"])
    #parser.add_argument("-ccd", "--crawl-delay", action="store", dest="cdelay", type=int, help=help_texts["cdelay"])
    #parser.add_argument("-ccr", "--crawl-random", action="store", dest="crandom", type=int, help=help_texts["crandm"])
    
    return parser.parse_known_args() 

main()
